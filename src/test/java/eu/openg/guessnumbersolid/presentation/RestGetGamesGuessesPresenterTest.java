package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter;
import eu.openg.guessnumbersolid.presentation.converter.GetGamesGuessesResponseConverter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.Null;
import spark.Response;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.GetGamesGuessesResponse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class RestGetGamesGuessesPresenterTest {
    private Response viewModel;
    private JSONTransformer jsonTransformer;
    private GetGamesGuessesPresenter presenter;
    private GetGamesGuessesResponseConverter getGamesGuessesResponseConverter;

    @Before
    public void setUp() {
        viewModel = mock(Response.class);
        jsonTransformer = mock(JSONTransformer.class);
        getGamesGuessesResponseConverter = mock(GetGamesGuessesResponseConverter.class);
        presenter = new RestGetGamesGuessesPresenter(viewModel, jsonTransformer, getGamesGuessesResponseConverter);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailOnNullGetGameResponse() {
        when(getGamesGuessesResponseConverter.convert(any())).thenThrow(NullPointerException.class);
        presenter.onSuccessfulGetGamesGuesses(null);
    }

    @Test
    public void shouldPresentGameNotFound() {
        presenter.onGameNotFound();

        verifyViewModelCalls(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS, "");
    }

    @Test
    public void shouldSuccessfullyFillViewModelWithGetGameResponse() {
        GetGamesGuessesResponse getGameResponse = mock(GetGamesGuessesResponse.class);
        when(jsonTransformer.toJSON(any())).thenReturn("test");

        presenter.onSuccessfulGetGamesGuesses(getGameResponse);

        verifyViewModelCalls(HttpProtocolUtil.SUCCESSFUL_GET_STATUS, "test");
    }

    private void verifyViewModelCalls(int contentNotFoundStatus, String s) {
        verify(viewModel, times(1)).status(contentNotFoundStatus);
        verify(viewModel, times(1)).body(s);
    }
}