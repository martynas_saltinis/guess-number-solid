package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.HttpException;
import eu.openg.guessnumbersolid.domain.usecase.GuessUseCase;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import spark.Request;

import static eu.openg.guessnumbersolid.domain.usecase.GuessUseCase.GuessRequest;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class GuessControllerTest {
    private Request request;
    private GuessUseCase guessUseCase;
    private GuessController guessController;
    private JSONTransformer jsonTransformer;
    private RestGuess restGuess;

    @Before
    public void setUp() {
        request = mock(Request.class);
        guessUseCase = mock(GuessUseCase.class);
        jsonTransformer = mock(JSONTransformer.class);
        restGuess = mock(RestGuess.class);
        guessController = new GuessController(guessUseCase, jsonTransformer);
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullRequest() {
        guessController.execute(null);
    }

    @Test(expected = HttpException.class)
    public void shouldFailGuessOnContentNotJSON() {
        when(request.body()).thenReturn("asdd");
        when(jsonTransformer.fromJSON(request.body(), RestGuess.class)).thenThrow(HttpException.class);
        guessController.execute(request);
    }

    @Test(expected = HttpException.class)
    public void shouldFailGuessOnInvalidGameIdFormat() {
        when(request.body()).thenReturn("{ \"number\": 1}");
        when(jsonTransformer.fromJSON(anyString(), any())).thenReturn(restGuess);
        setupRequestWithGameId("as4w");

        guessController.execute(request);
    }

    @Test
    public void shouldCallUseCaseSuccessfully() {
        long gameId = 1;
        int guessNumber = 0;
        setupRequestWithGameId(String.valueOf(gameId));
        when(request.body()).thenReturn("");
        when(jsonTransformer.fromJSON(any(), any())).thenReturn(restGuess);

        guessController.execute(request);

        ArgumentCaptor<GuessRequest> captor = ArgumentCaptor.forClass(GuessRequest.class);
        verify(guessUseCase, times(1)).execute(captor.capture());
        assertGuessRequestFields(gameId, guessNumber, captor.getValue());
    }

    private void assertGuessRequestFields(long gameId, int guessNumber, GuessRequest value) {
        assertEquals(gameId, value.getGameId());
        assertEquals(guessNumber, value.getGuess());
    }

    private void setupRequestWithGameId(String id) {
        when(request.contentType()).thenReturn(HttpProtocolUtil.CONTENT_TYPE_JSON);
        when(request.params("id")).thenReturn(id);
    }
}
