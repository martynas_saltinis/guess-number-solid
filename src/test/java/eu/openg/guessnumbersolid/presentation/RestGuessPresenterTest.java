package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.presentation.converter.GuessResponseConverter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import org.junit.Before;
import org.junit.Test;
import spark.Response;

import static eu.openg.guessnumbersolid.domain.usecase.GuessPresenter.GuessResponse;
import static org.mockito.Mockito.*;

public class RestGuessPresenterTest {
    private Response viewModel;
    private JSONTransformer jsonTransformer;
    private RestGuessPresenter presenter;
    private GuessResponseConverter guessResponseConverter;

    @Before
    public void setUp() {
        viewModel = mock(Response.class);
        jsonTransformer = mock(JSONTransformer.class);
        guessResponseConverter = mock(GuessResponseConverter.class);
        presenter = new RestGuessPresenter(viewModel, jsonTransformer, guessResponseConverter);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionOnNullGuessResult() {
        when(guessResponseConverter.convert(any())).thenThrow(NullPointerException.class);
        presenter.onSuccessfulGuess(null);
    }

    @Test
    public void shouldFillViewModelWithSuccessfulGuessResult() {
        GuessResponse guessResponse = mock(GuessResponse.class);
        String body = "test";
        setupJSONTransformerWithBody(body);

        presenter.onSuccessfulGuess(guessResponse);

        verifyHttpResponseJSON(HttpProtocolUtil.SUCCESSFUL_POST_STATUS, body);
    }

    @Test
    public void shouldFillViewModelWithContentNotFoundResponse() {
        presenter.onGameNotFound();

        verifyHttpResponseJSON(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS, "");
    }

    private void setupJSONTransformerWithBody(String body) {
        when(jsonTransformer.toJSON(any())).thenReturn(body);
    }

    private void verifyHttpResponseJSON(int status, String body) {
        verify(viewModel, times(1)).status(status);
        verify(viewModel, times(1)).body(body);
    }
}