package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.StartGamePresenter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import org.junit.Before;
import org.junit.Test;
import spark.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class RestStartGamePresenterTest {
    private RestStartGamePresenter presenter;
    private Response viewModel;
    private JSONTransformer jsonTransformer;

    @Before
    public void setUp() {
        viewModel = mock(Response.class);
        jsonTransformer = mock(JSONTransformer.class);
        presenter = new RestStartGamePresenter(viewModel, jsonTransformer);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionOnNullGameStartInfo() {
        when(jsonTransformer.toJSON(any())).thenThrow(NullPointerException.class);
        presenter.onGameStart(null);
    }


    @Test
    public void shouldFillViewModelWithSuccessfulGameStartInfo() {
        StartGamePresenter.StartGameResponse startGameResponse = mock(StartGamePresenter.StartGameResponse.class);
        String body = "test";
        setupJSONTransformerWithBody(body);

        presenter.onGameStart(startGameResponse);

        verifyHttpResponseJSON(body);
    }

    private void setupJSONTransformerWithBody(String body) {
        when(jsonTransformer.toJSON(any())).thenReturn(body);
    }

    private void verifyHttpResponseJSON(String body) {
        verify(viewModel, times(1)).status(HttpProtocolUtil.SUCCESSFUL_POST_STATUS);
        verify(viewModel, times(1)).body(body);
    }
}
