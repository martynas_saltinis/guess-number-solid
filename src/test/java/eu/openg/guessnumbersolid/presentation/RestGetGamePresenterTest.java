package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import org.junit.Before;
import org.junit.Test;
import spark.Response;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter.GetGameResponse;
import static org.mockito.Mockito.*;

public class RestGetGamePresenterTest {
    private Response viewModel;
    private JSONTransformer jsonTransformer;
    private RestGetGamePresenter presenter;

    @Before
    public void setUp() {
        viewModel = mock(Response.class);
        jsonTransformer = mock(JSONTransformer.class);
        presenter = new RestGetGamePresenter(viewModel, jsonTransformer);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailOnNullGetGameResponse() {
        presenter.onSuccessfulGetGame(null);
    }

    @Test
    public void shouldPresentGameNotFound() {
        presenter.onGameNotFound();

        verifyViewModelCalls(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS, "");
    }

    @Test
    public void shouldSuccessfullyFillViewModelWithGetGameResponse() {
        GetGameResponse getGameResponse = mock(GetGameResponse.class);
        when(jsonTransformer.toJSON(any())).thenReturn("test");

        presenter.onSuccessfulGetGame(getGameResponse);

        verifyViewModelCalls(HttpProtocolUtil.SUCCESSFUL_GET_STATUS, "test");
    }

    private void verifyViewModelCalls(int status, String body) {
        verify(viewModel, times(1)).status(status);
        verify(viewModel, times(1)).body(body);
    }

}