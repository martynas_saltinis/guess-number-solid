package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.HttpException;
import eu.openg.guessnumbersolid.domain.usecase.GetGameUseCase;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import org.junit.Before;
import org.junit.Test;
import spark.Request;

import static org.mockito.Mockito.*;

public class GetGameControllerTest {
    private Request request;
    private GetGameUseCase getGameUseCase;
    private GetGameController getGameController;

    @Before
    public void setUp() {
        request = mock(Request.class);
        getGameUseCase = mock(GetGameUseCase.class);
        getGameController = new GetGameController(getGameUseCase);
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullRequest() {
        getGameController.execute(null);
    }

    @Test(expected = HttpException.class)
    public void shouldFailGuessOnInvalidGameIdFormat() {
        setupRequestWithGameId("as4w");

        getGameController.execute(request);
    }

    @Test
    public void shouldSuccessfullyCallUseCaseAndResponseBody() {
        setupRequestWithGameId("1");

        getGameController.execute(request);

        verify(getGameUseCase, times(1)).execute(1);
    }

    private void setupRequestWithGameId(String id) {
        when(request.contentType()).thenReturn(HttpProtocolUtil.CONTENT_TYPE_JSON);
        when(request.params("id")).thenReturn(id);
    }
}
