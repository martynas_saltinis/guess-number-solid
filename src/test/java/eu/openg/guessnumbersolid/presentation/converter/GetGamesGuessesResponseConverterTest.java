package eu.openg.guessnumbersolid.presentation.converter;

import eu.openg.guessnumbersolid.presentation.GuessAnswerParser;
import eu.openg.guessnumbersolid.presentation.converter.GetGamesGuessesResponseConverter.FormattedGetGamesGuessesResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.GetGamesGuessesResponse;
import static eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.Guess;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetGamesGuessesResponseConverterTest {
    private GuessAnswerParser guessAnswerParser;
    private GetGamesGuessesResponseConverter converter;
    private GetGamesGuessesResponse response;

    @Before
    public void setUp() {
        response = mock(GetGamesGuessesResponse.class);
        guessAnswerParser = mock(GuessAnswerParser.class);
        converter = new GetGamesGuessesResponseConverter(guessAnswerParser);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerException_WhenInputIsNull() {
        converter.convert(null);
    }

    @Test
    public void shouldConvertResponseWithEmptyGuessList() {
        when(response.getGuesses()).thenReturn(new ArrayList<>());

        FormattedGetGamesGuessesResponse result = converter.convert(response);

        Assert.assertEquals(0, result.getGuesses().size());
    }

    @Test
    public void shouldCovertResponseWithGuesses() {
        when(response.getGuesses()).thenReturn(Arrays.asList(new Guess(1, 1, 0)));
        when(guessAnswerParser.invoke(anyInt())).thenReturn("Correct");

        FormattedGetGamesGuessesResponse result = converter.convert(response);

        Assert.assertEquals(1, result.getGuesses().size());
        List<GetGamesGuessesResponseConverter.FormattedGuess> guesses = result.getGuesses();
        assertFormattedGuess(guesses);
    }

    private void assertFormattedGuess(List<GetGamesGuessesResponseConverter.FormattedGuess> guesses) {
        Assert.assertEquals(1, guesses.get(0).getId());
        Assert.assertEquals("Correct", guesses.get(0).getAnswer());
        Assert.assertEquals(1, guesses.get(0).getNumber());
    }
}