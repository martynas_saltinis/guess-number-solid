package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.HttpException;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesUseCase;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import org.junit.Before;
import org.junit.Test;
import spark.Request;

import static org.mockito.Mockito.*;

public class GetGamesGuessesControllerTest {
    private Request request;
    private GetGamesGuessesUseCase getGamesGuessesUseCase;
    private GetGamesGuessesController getGamesGuessesController;

    @Before
    public void setUp() {
        request = mock(Request.class);
        getGamesGuessesUseCase = mock(GetGamesGuessesUseCase.class);
        getGamesGuessesController = new GetGamesGuessesController(getGamesGuessesUseCase);
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowNullRequest() {
        getGamesGuessesController.execute(null);
    }

    @Test(expected = HttpException.class)
    public void shouldFailGuessOnInvalidGameIdFormat() {
        setupRequestWithGameId("as4w");

        getGamesGuessesController.execute(request);
    }

    @Test
    public void shouldSuccessfullyCallUseCase() {
        setupRequestWithGameId("1");
        getGamesGuessesController.execute(request);
        verify(getGamesGuessesUseCase, times(1)).execute(1);
    }

    private void setupRequestWithGameId(String id) {
        when(request.contentType()).thenReturn(HttpProtocolUtil.CONTENT_TYPE_JSON);
        when(request.params("id")).thenReturn(id);
    }
}