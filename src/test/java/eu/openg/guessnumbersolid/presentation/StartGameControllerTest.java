package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import eu.openg.guessnumbersolid.utils.JavaRandomNumberGenerator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import spark.Request;

import static eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase.StartGameRequest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class StartGameControllerTest {
    private StartGameController startGameController;
    private StartGameUseCase startGameUseCase;
    private Request request;
    private JSONTransformer jsonTransformer;
    private RestStartGame restStartGame;

    @Before
    public void setUp() {
        request = mock(Request.class);
        restStartGame = mock(RestStartGame.class);
        startGameUseCase = mock(StartGameUseCase.class);
        jsonTransformer = mock(JSONTransformer.class);
        startGameController = new StartGameController(startGameUseCase, jsonTransformer);
    }

    @Test
    public void shouldCallStartGameUseCase() {
        setupStartRequestDeserialization(1, 10);
        ArgumentCaptor<StartGameRequest> captor = ArgumentCaptor.forClass(StartGameRequest.class);

        startGameController.execute(request);

        verify(startGameUseCase, times(1)).startGame(captor.capture());
        verifyUseCaseCallParameters(captor.getValue());
    }

    @Test(expected = InvalidRequestNumberInterval.class)
    public void shouldReceiveException_CallingBoundaryWithIllegalInterval() {
        setupStartRequestDeserialization(10, 1);
        doThrow(JavaRandomNumberGenerator.InvalidIntervalSequence.class).when(startGameUseCase).startGame(any());

        startGameController.execute(request);
    }

    private void verifyUseCaseCallParameters(StartGameRequest value) {
        assertEquals(1, value.getNumberFrom());
        assertEquals(10, value.getNumberTo());
    }

    private void setupStartRequestDeserialization(int numberFrom, int numberTo) {
        when(restStartGame.getNumberFrom()).thenReturn(numberFrom);
        when(restStartGame.getNumberTo()).thenReturn(numberTo);
        when(request.body()).thenReturn("");
        when(jsonTransformer.fromJSON(anyString(), any())).thenReturn(restStartGame);
    }

}
