package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter.GetGameResponse;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class GetGameInteractorTest {
    private GameDataAccess gameDataAccess;
    private GetGamePresenter getGamePresenter;
    private GetGameInteractor getGameInteractor;

    @Before
    public void setUp() {
        gameDataAccess = mock(GameDataAccess.class);
        getGamePresenter = mock(GetGamePresenter.class);
        getGameInteractor = new GetGameInteractor(getGamePresenter, gameDataAccess);
    }

    @Test
    public void shouldNotFindGame() {
        getGameInteractor.execute(-1);

        verify(gameDataAccess, times(1)).getGameById(-1);
        verify(getGamePresenter, times(1)).onGameNotFound();
    }

    @Test
    public void shouldSuccessfullyReturnGameInfo() {
        int numberOfGuesses = 1;
        int answerFrom = 1;
        int answerTo = 10;
        Game game = setupGameMock(answerFrom, answerTo);
        when(gameDataAccess.getGameById(anyLong())).thenReturn(Optional.of(game));

        getGameInteractor.execute(-1);

        ArgumentCaptor<GetGameResponse> responseCaptor = ArgumentCaptor.forClass(GetGameResponse.class);
        verify(getGamePresenter, times(1)).onSuccessfulGetGame(responseCaptor.capture());
        GetGameResponse actualResponse = responseCaptor.getValue();
        assertResponse(numberOfGuesses, answerFrom, answerTo, actualResponse);
    }

    private void assertResponse(int numberOfGuesses, int answerFrom, int answerTo, GetGameResponse actualResponse) {
        Assert.assertEquals(numberOfGuesses, actualResponse.getNumberOfGuesses());
        Assert.assertEquals(answerFrom, actualResponse.getGuessFrom());
        Assert.assertEquals(answerTo, actualResponse.getGuessTo());
    }

    private Game setupGameMock(int answerFrom, int answerTo) {
        Game game = mock(Game.class);
        List<Guess> guesses = Arrays.asList(new Guess(1, 1));
        when(game.getGuesses()).thenReturn(guesses);
        when(game.getAnswerFrom()).thenReturn(answerFrom);
        when(game.getAnswerTo()).thenReturn(answerTo);
        return game;
    }
}