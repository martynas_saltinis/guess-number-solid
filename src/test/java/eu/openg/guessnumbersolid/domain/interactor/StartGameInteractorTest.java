package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.usecase.StartGamePresenter;
import eu.openg.guessnumbersolid.utils.AnswerGenerator;
import org.junit.Before;
import org.junit.Test;

import static eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase.StartGameRequest;
import static org.mockito.Mockito.*;

public class StartGameInteractorTest {
    private StartGameInteractor startGameInteractor;
    private StartGamePresenter startGamePresenter;
    private GameDataAccess gameDataAccess;
    private AnswerGenerator answerGenerator;

    @Before
    public void setUp() {
        gameDataAccess = mock(GameDataAccess.class);
        startGamePresenter = mock(StartGamePresenter.class);
        answerGenerator = mock(AnswerGenerator.class);
        startGameInteractor = new StartGameInteractor(startGamePresenter, gameDataAccess, answerGenerator);
    }

    @Test
    public void shouldCreateGameAndCallStartGameOutputBoundary() {
        when(answerGenerator.generateAnswer(anyInt(), anyInt())).thenReturn(5);

        startGameInteractor.startGame(createMockRequest());

        verify(answerGenerator, times(1)).generateAnswer(anyInt(), anyInt());
        verify(gameDataAccess, times(1)).createNewGame(5, 1, 10);
        verify(startGamePresenter, times(1)).onGameStart(any());
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenDataStorageIsNull() {
        startGameInteractor = new StartGameInteractor(startGamePresenter, null, answerGenerator);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenPresenterIsNull() {
        startGameInteractor = new StartGameInteractor(null, gameDataAccess, answerGenerator);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenAnswerGeneratorIsNull() {
        startGameInteractor = new StartGameInteractor(startGamePresenter, gameDataAccess, null);
    }

    private StartGameRequest createMockRequest() {
        StartGameRequest request = mock(StartGameRequest.class);
        when(request.getNumberFrom()).thenReturn(1);
        when(request.getNumberTo()).thenReturn(10);
        return request;
    }
}