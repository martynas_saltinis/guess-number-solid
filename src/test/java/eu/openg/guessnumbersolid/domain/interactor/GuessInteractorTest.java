package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.usecase.GuessPresenter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Optional;

import static eu.openg.guessnumbersolid.domain.usecase.GuessPresenter.GuessResponse;
import static eu.openg.guessnumbersolid.domain.usecase.GuessUseCase.GuessRequest;
import static org.mockito.Mockito.*;

public class GuessInteractorTest {
    private GameDataAccess gameDataAccess;
    private GuessPresenter guessPresenter;
    private GuessInteractor guessInteractor;
    private Game game;

    @Before
    public void setUp() {
        game = mock(Game.class);
        gameDataAccess = mock(GameDataAccess.class);
        guessPresenter = mock(GuessPresenter.class);
        guessInteractor = new GuessInteractor(guessPresenter, gameDataAccess);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenDataStorageIsNull() {
        guessInteractor = new GuessInteractor(guessPresenter, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenOutputBoundaryIsNull() {
        guessInteractor = new GuessInteractor(null, gameDataAccess);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenGuessInputIsNull() {
        guessInteractor.execute(null);
    }

    @Test
    public void shouldNotFindGame() {
        guessInteractor.execute(mock(GuessRequest.class));

        verify(guessPresenter, times(1)).onGameNotFound();
    }

    @Test
    public void shouldReturnIncorrectGuess() {
        mockGuessResultAndAssertResult(Game.GuessAnswer.GUESS_LOWER);
    }

    @Test
    public void shouldReturnCorrectGuess() {
        mockGuessResultAndAssertResult(Game.GuessAnswer.CORRECT);
    }

    private void mockGuessResultAndAssertResult(Game.GuessAnswer answer) {
        Mockito.when(game.guess(any())).thenReturn(answer);
        Mockito.when(gameDataAccess.getGameById(anyLong())).thenReturn(Optional.of(game));

        guessInteractor.execute(mock(GuessRequest.class));

        makeGuessAndAssertBoundaryCallWithResult(answer);
    }

    private void makeGuessAndAssertBoundaryCallWithResult(Game.GuessAnswer answer) {
        ArgumentCaptor<GuessResponse> guessResultCaptor = ArgumentCaptor.forClass(GuessResponse.class);
        verify(gameDataAccess, times(1)).saveGame(game);
        verify(guessPresenter, times(1)).onSuccessfulGuess(guessResultCaptor.capture());
        GuessResponse actualGuessResponse = guessResultCaptor.getValue();
        Assert.assertEquals(answer.getValue(), actualGuessResponse.getAnswer());
    }
}