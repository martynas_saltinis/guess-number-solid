package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.GetGamesGuessesResponse;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class GetGamesGuessesInteractorTest {
    private GetGamesGuessesPresenter presenter;
    private GameDataAccess gameDataAccess;
    private GetGamesGuessesInteractor useCase;

    @Before
    public void setUp() {
        presenter = mock(GetGamesGuessesPresenter.class);
        gameDataAccess = mock(GameDataAccess.class);
        useCase = new GetGamesGuessesInteractor(presenter, gameDataAccess);
    }

    @Test
    public void shouldCallPresentersGameNotFound() {
        useCase.execute(0);

        verify(presenter, times(1)).onGameNotFound();
    }

    @Test
    public void shouldReturnGuessesCollection() {
        Game game = mock(Game.class);
        List<Guess> guessList = new ArrayList<>();
        when(gameDataAccess.getGameById(anyLong())).thenReturn(Optional.of(game));
        when(game.getGuesses()).thenReturn(guessList);
        ArgumentCaptor<GetGamesGuessesPresenter.GetGamesGuessesResponse> responseCaptor = ArgumentCaptor.forClass(GetGamesGuessesResponse.class);

        useCase.execute(0);

        verify(presenter, times(1)).onSuccessfulGetGamesGuesses(responseCaptor.capture());
        GetGamesGuessesResponse actualResponse = responseCaptor.getValue();
        Assert.assertEquals(0, actualResponse.getGuesses().size());
    }
}