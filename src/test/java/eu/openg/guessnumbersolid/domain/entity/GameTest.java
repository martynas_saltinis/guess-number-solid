package eu.openg.guessnumbersolid.domain.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GameTest {
    private Game game;
    private Guess guess;

    @Before
    public void setUp() {
        game = new Game(1, 5, 1, 10);
        guess = mock(Guess.class);
    }

    private void setMockGuessNumber(int mockNumber) {
        when(guess.getNumber()).thenReturn(mockNumber);
    }

    @Test
    public void shouldGuessInCorrectly() {
        setMockGuessNumber(3);
        Assert.assertEquals(1, game.guess(guess).getValue());
    }

    @Test
    public void shouldCheckThatGuessIsIncorrect() {
        Assert.assertEquals(1, game.getAnswerToGuess(new Guess(1, 4)).getValue());
        Assert.assertEquals(-1, game.getAnswerToGuess(new Guess(1, 6)).getValue());
    }

    @Test(expected = Game.GuessAlreadyExistsException.class)
    public void shouldNotLetToAddAlreadyExistingGuess() {
        List<Guess> guesses = new ArrayList<>();
        guesses.add(new Guess(1, 1));
        game = new Game(1, 5, 1, 10, guesses);
        game.guess(new Guess(1, 1));
    }

    @Test
    public void shouldIncrementGuessCountAfterGuess() {
        int initialGuessCount = game.getGuesses().size();
        setMockGuessNumber(3);

        game.guess(guess);

        int finalGuessCount = game.getGuesses().size();
        assertEquals(initialGuessCount + 1, finalGuessCount);
    }
}