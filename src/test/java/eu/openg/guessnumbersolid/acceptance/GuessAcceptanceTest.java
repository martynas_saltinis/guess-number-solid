package eu.openg.guessnumbersolid.acceptance;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.openg.guessnumbersolid.ApplicationRunner;
import eu.openg.guessnumbersolid.Config;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.HttpRequestFormer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class GuessAcceptanceTest {
    private ApplicationRunner applicationRunner;
    private int port;
    private HttpRequestFormer httpRequestFormer;

    @Before
    public void setUp() {
        List<Game> games = createGames();
        Config config = new Config(games);
        applicationRunner = new ApplicationRunner();
        applicationRunner.start(0, config);
        httpRequestFormer = new HttpRequestFormer();
        port = applicationRunner.getPort();
    }

    @Test
    public void shouldGuessIncorrectly() throws UnirestException {
        makeGuessAndAssertResult(1, "Guess higher");
    }

    @Test
    public void shouldGuessCorrectly() throws UnirestException {
        makeGuessAndAssertResult(5, "Correct");
    }

    @Test
    public void shouldGetGameNotFoundWhenMakingAGuessForNotExistingGame() throws UnirestException {
        HttpResponse<String> response = httpRequestFormer.postGuessToGame(port, 3, 5).asString();
        Assert.assertEquals(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS, response.getStatus());
    }

    @After
    public void tearDown() {
        applicationRunner.stop();
    }

    private List<Game> createGames() {
        return Arrays.asList(new Game(1, 5, 1, 10),
                new Game(2, 5, 1, 10));
    }

    private void assertGuessResultJsonResponse(HttpResponse<JsonNode> response, String answer) {
        Assert.assertEquals(HttpProtocolUtil.SUCCESSFUL_POST_STATUS, response.getStatus());
        Assert.assertEquals(answer, response.getBody().getObject().getString("answer"));
        Assert.assertEquals(1, response.getBody().getObject().getInt("numberOfGuesses"));
    }

    private void makeGuessAndAssertResult(int guess, String answer) throws UnirestException {
        HttpResponse<JsonNode> response = httpRequestFormer.postGuessToGame(port, (long) 1, guess).asJson();
        assertGuessResultJsonResponse(response, answer);
    }
}

