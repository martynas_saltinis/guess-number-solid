package eu.openg.guessnumbersolid.acceptance;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.openg.guessnumbersolid.ApplicationRunner;
import eu.openg.guessnumbersolid.Config;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.HttpRequestFormer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GetGameAcceptanceTest {
    private ApplicationRunner applicationRunner;
    private int port;
    private HttpRequestFormer httpRequestFormer;

    @Before
    public void setUp() {
        List<Game> games = createGames();
        Config config = new Config(games);
        applicationRunner = new ApplicationRunner();
        applicationRunner.start(0, config);
        httpRequestFormer = new HttpRequestFormer();
        port = applicationRunner.getPort();
    }

    @Test
    public void shouldGetGameSuccessfully() throws UnirestException {
        HttpResponse<JsonNode> response = httpRequestFormer.getGame(port, 1).asJson();
        Assert.assertEquals(HttpProtocolUtil.SUCCESSFUL_GET_STATUS, response.getStatus());
        Assert.assertEquals(1, response.getBody().getObject().getInt("guessFrom"));
        Assert.assertEquals(10, response.getBody().getObject().getInt("guessTo"));
        Assert.assertEquals(1, response.getBody().getObject().getInt("numberOfGuesses"));
    }

    @Test
    public void shouldNotFindGame() throws UnirestException {
        HttpResponse<JsonNode> response = httpRequestFormer.getGame(port, 0).asJson();
        Assert.assertEquals(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS, response.getStatus());
        Assert.assertEquals("{}", response.getBody().toString());
    }


    @After
    public void tearDown() {
        applicationRunner.stop();
    }

    private List<Game> createGames() {
        List<Game> games = new ArrayList<>();
        List<Guess> guesses = new ArrayList<>();
        Guess guess = new Guess(1, 1);
        guesses.add(guess);
        games.add(new Game(1, 5, 1, 10, guesses));
        games.add(new Game(2, 5, 1, 10, guesses));
        return games;
    }
}
