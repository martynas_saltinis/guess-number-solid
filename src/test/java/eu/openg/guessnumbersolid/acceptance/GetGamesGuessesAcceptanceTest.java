package eu.openg.guessnumbersolid.acceptance;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.openg.guessnumbersolid.ApplicationRunner;
import eu.openg.guessnumbersolid.Config;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.HttpRequestFormer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetGamesGuessesAcceptanceTest {
    private ApplicationRunner applicationRunner;
    private int port;
    private HttpRequestFormer httpRequestFormer;

    @Before
    public void setUp() {
        List<Guess> guesses = Arrays.asList(new Guess(1, 1));
        List<Game> games = Arrays.asList(new Game(1, 5, 1, 10, guesses),
                                         new Game(2, 5, 1, 10));
        Config config = new Config(games);
        applicationRunner = new ApplicationRunner();
        applicationRunner.start(0, config);
        httpRequestFormer = new HttpRequestFormer();
        port = applicationRunner.getPort();
    }

    @Test
    public void shouldNotFindGame() throws UnirestException {
        HttpResponse<JsonNode> response = httpRequestFormer.getGamesGuesses(port, 0).asJson();
        Assert.assertEquals(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS, response.getStatus());
        Assert.assertEquals(new JsonNode("").toString(), response.getBody().toString());
    }

    @Test
    public void shouldReturnEmptyGuessList() throws UnirestException {
        makeRequestWithGameIdAndAssertGuessesCount(2, 0);
    }

    @Test
    public void shouldReturnGuessListWithOneElement() throws UnirestException {
        makeRequestWithGameIdAndAssertGuessesCount(1, 1);
    }

    @After
    public void tearDown() {
        applicationRunner.stop();
    }

    private void makeRequestWithGameIdAndAssertGuessesCount(long gameId, int guessesCount) throws UnirestException {
        HttpResponse<JsonNode> response = httpRequestFormer.getGamesGuesses(port, gameId).asJson();
        Assert.assertEquals(HttpProtocolUtil.SUCCESSFUL_GET_STATUS, response.getStatus());
        Assert.assertEquals(guessesCount, response.getBody().getObject().getJSONArray("guesses").length());
    }
}
