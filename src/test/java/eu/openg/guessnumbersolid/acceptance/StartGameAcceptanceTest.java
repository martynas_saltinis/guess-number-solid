package eu.openg.guessnumbersolid.acceptance;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.openg.guessnumbersolid.ApplicationRunner;
import eu.openg.guessnumbersolid.Config;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.HttpRequestFormer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StartGameAcceptanceTest {
    private int port;
    private ApplicationRunner applicationRunner;
    private HttpRequestFormer httpRequestFormer;


    @Before
    public void setUp() {
        List<Game> games = createGames();
        Config config = new Config(games);
        applicationRunner = new ApplicationRunner();
        applicationRunner.start(0, config);
        httpRequestFormer = new HttpRequestFormer();
        port = applicationRunner.getPort();
    }

    @Test
    public void shouldStartGameAndGetGameInfo() throws UnirestException {
        int from = 5;
        int to = 15;
        HttpResponse<JsonNode> response = httpRequestFormer.postGame(port, from, to).asJson();
        Assert.assertEquals(HttpProtocolUtil.SUCCESSFUL_POST_STATUS, response.getStatus());
        Assert.assertEquals(3, response.getBody().getObject().getInt("gameId"));
        Assert.assertEquals(from, response.getBody().getObject().getInt("guessFrom"));
        Assert.assertEquals(to, response.getBody().getObject().getInt("guessTo"));
    }

    @After
    public void tearDown() {
        applicationRunner.stop();
    }

    private List<Game> createGames() {
        List<Game> games = new ArrayList<>();
        List<Guess> guesses = Arrays.asList(new Guess(1, 1));
        games.add(new Game(1, 5, 1, 10, guesses));
        games.add(new Game(2, 5, 1, 10, guesses));
        return games;
    }
}
