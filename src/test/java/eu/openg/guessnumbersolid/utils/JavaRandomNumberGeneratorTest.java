package eu.openg.guessnumbersolid.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class JavaRandomNumberGeneratorTest {
    private JavaRandomNumberGenerator numberGenerator;

    @Before
    public void setUp() {
        numberGenerator = new JavaRandomNumberGenerator();
    }

    @Test(expected = JavaRandomNumberGenerator.InvalidIntervalSequence.class)
    public void shouldThrowExceptionWhenFromIsBiggerThanTo() {
        numberGenerator.generateAnswer(6, 5);
    }

    @Test
    public void shouldReturn1_WhenFromIs1AndToIs2() {
        Assert.assertEquals(1, numberGenerator.generateAnswer(1, 1));
    }
}