package eu.openg.guessnumbersolid.data;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class InMemoryGameDataAccessTest {
    private GameDataAccess gameDataAccess;

    @Before
    public void setUp() {
        List<Game> games = new ArrayList<>();
        List<Guess> guesses = Arrays.asList(new Guess(1, 1));
        games.add(new Game(1, 5, 1, 10, guesses));
        games.add(new Game(2, 5, 1, 10, guesses));
        gameDataAccess = new InMemoryGameDataAccess(games);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionOnNullList() {
        new InMemoryGameDataAccess(null);
    }

    @Test
    public void shouldCreateGameWithId3() {
        assertEquals(3, gameDataAccess.createNewGame(5, 1, 10));
    }

    @Test
    public void shouldFindGame() {
        Assert.assertNotNull(gameDataAccess.getGameById(1));
    }

    @Test
    public void shouldNotFindGame() {
        assertEquals(Optional.empty(), gameDataAccess.getGameById(3));
    }

    @Test
    public void shouldSaveGameWithSameIdAndOneGuess() {
        List<Game> games = Arrays.asList(new Game(1, 5, 1, 10));
        gameDataAccess = new InMemoryGameDataAccess(games);

        gameDataAccess.saveGame(new Game(1, 5, 1, 10, Arrays.asList(new Guess(1,1))));

        Assert.assertEquals(1, games.get(0).getGuesses().size());
    }
}