package eu.openg.guessnumbersolid.utils;

public class HttpProtocolUtil {
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final int SUCCESSFUL_POST_STATUS = 201;
    public static final int CONTENT_NOT_FOUND_STATUS = 404;
    public static final int BAD_REQUEST_STATUS = 400;
    public static final int SUCCESSFUL_GET_STATUS = 200;

    private HttpProtocolUtil() {
    }
}
