package eu.openg.guessnumbersolid.utils;

import java.util.concurrent.ThreadLocalRandom;

public class JavaRandomNumberGenerator implements AnswerGenerator {
    @Override
    public int generateAnswer(int fromInclusive, int toInclusive) {
        if (fromInclusive == toInclusive)
            return fromInclusive;
        if (fromInclusive > toInclusive)
            throw new InvalidIntervalSequence(fromInclusive, toInclusive);
        return ThreadLocalRandom.current().nextInt(fromInclusive, toInclusive + 1);
    }

    public class InvalidIntervalSequence extends RuntimeException {
        private int intervalStart;
        private int intervalEnd;

        public InvalidIntervalSequence(int intervalStart, int intervalEnd) {
            super(String.format("Interval start (%d) is bigger than the end (%d)", intervalStart, intervalEnd));
            this.intervalStart = intervalStart;
            this.intervalEnd = intervalEnd;
        }

        public int getIntervalStart() {
            return intervalStart;
        }

        public int getIntervalEnd() {
            return intervalEnd;
        }
    }
}
