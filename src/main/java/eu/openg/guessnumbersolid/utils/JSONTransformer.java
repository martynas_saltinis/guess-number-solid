package eu.openg.guessnumbersolid.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.openg.guessnumbersolid.HttpException;

import java.io.IOException;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class JSONTransformer {
    private final ObjectMapper mapper;

    public JSONTransformer() {
        this.mapper = setUpMapper();
    }

    public String toJSON(Object originalObject) {
        try {
            return mapper.writeValueAsString(originalObject);
        } catch (IOException e) {
            throw new JSONProcessingException(e);
        }
    }

    public <T> T fromJSON(String original, Class<T> objectClass) {
        try {
            return objectClass.cast(mapper.readValue(original, objectClass));
        } catch (IOException e) {
            throw new JSONProcessingException(e);
        }
    }

    private ObjectMapper setUpMapper() {
        ObjectMapper om = new ObjectMapper();
        om.configure(FAIL_ON_UNKNOWN_PROPERTIES, true);
        om.setSerializationInclusion(NON_NULL);
        return om;
    }

    public class JSONProcessingException extends HttpException {
        JSONProcessingException(IOException e) {
            super(HttpProtocolUtil.BAD_REQUEST_STATUS, e);
        }
    }
}
