package eu.openg.guessnumbersolid.utils;

public interface AnswerGenerator {
    int generateAnswer(int fromInclusive, int toInclusive);
}
