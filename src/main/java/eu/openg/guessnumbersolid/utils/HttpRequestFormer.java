package eu.openg.guessnumbersolid.utils;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.BaseRequest;
import com.mashape.unirest.request.body.RequestBodyEntity;

public class HttpRequestFormer {
    private final String DOMAIN = "http://localhost";

    public RequestBodyEntity postGame(int port, int numberFrom, int numberTo) {
        return Unirest.post(DOMAIN + ":" + port + "/games")
                .header("content-type", HttpProtocolUtil.CONTENT_TYPE_JSON)
                .body("{ \"numberFrom\": " + numberFrom + "," +
                        "\"numberTo\"  : " + numberTo + "} ");
    }

    public RequestBodyEntity postGuessToGame(int port, long gameId, int number) {
        return Unirest.post(DOMAIN + ":" + port + "/games/" + gameId + "/guesses")
                .header("content-type", HttpProtocolUtil.CONTENT_TYPE_JSON)
                .body("{ \"number\": " + number + "} ");
    }

    public BaseRequest getGame(int port, long gameId) {
        return Unirest.get(DOMAIN + ":" + port + "/games/" + gameId);
    }

    public BaseRequest getGamesGuesses(int port, long gameId) {
        return Unirest.get(DOMAIN + ":" + port + "/games/" + gameId + "/guesses");
    }
}
