package eu.openg.guessnumbersolid;

import eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter;
import eu.openg.guessnumbersolid.domain.usecase.GuessPresenter;
import eu.openg.guessnumbersolid.presentation.*;
import eu.openg.guessnumbersolid.presentation.converter.GetGamesGuessesResponseConverter;
import eu.openg.guessnumbersolid.presentation.converter.GuessResponseConverter;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import spark.Response;

public class PresenterBuilder {
    private final JSONTransformer jsonTransformer;
    private final GuessAnswerParser guessAnswerParser;

    public PresenterBuilder() {
        jsonTransformer = new JSONTransformer();
        guessAnswerParser = new GuessAnswerParser();
    }

    public RestStartGamePresenter createStartGamePresenter(Response viewModel) {
        return new RestStartGamePresenter(viewModel, jsonTransformer);
    }

    public GuessPresenter createGuessPresenter(Response viewModel) {
        return new RestGuessPresenter(viewModel, jsonTransformer, new GuessResponseConverter(guessAnswerParser));
    }

    public GetGamePresenter createGetGamePresenter(Response viewModel) {
        return new RestGetGamePresenter(viewModel, jsonTransformer);
    }

    public GetGamesGuessesPresenter createGetGamesGuessesPresenter(Response viewModel) {
        return new RestGetGamesGuessesPresenter(viewModel, jsonTransformer, new GetGamesGuessesResponseConverter(guessAnswerParser));
    }
}
