package eu.openg.guessnumbersolid.data;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class InMemoryGameDataAccess implements GameDataAccess {
    private final List<Game> games;

    public InMemoryGameDataAccess(List<Game> games) {
        this.games = requireNonNull(games);
    }

    @Override
    public long createNewGame(int answer, int answerFrom, int answerTo) {
        long newId = getNewGameId();
        games.add(new Game(newId, answer, answerFrom, answerTo));
        return newId;
    }

    @Override
    public Optional<Game> getGameById(long gameId) {
        return games.stream().filter(game -> game.getId() == gameId).findAny();
    }

    @Override
    public void saveGame(Game game) {
        for(int i = 0; i < games.size(); i++){
            if (games.get(i).getId() == game.getId())
                games.set(i, game);
        }
    }

    private long getNewGameId() {
        return games.size() + 1;
    }
}
