package eu.openg.guessnumbersolid;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.interactor.GetGameInteractor;
import eu.openg.guessnumbersolid.domain.interactor.GetGamesGuessesInteractor;
import eu.openg.guessnumbersolid.domain.interactor.GuessInteractor;
import eu.openg.guessnumbersolid.domain.interactor.StartGameInteractor;
import eu.openg.guessnumbersolid.domain.usecase.*;
import eu.openg.guessnumbersolid.utils.JavaRandomNumberGenerator;

public class UseCaseBuilder {
    private final GameDataAccess gameDataAccess;

    public UseCaseBuilder(GameDataAccess gameDataAccess) {
        this.gameDataAccess = gameDataAccess;
    }

    public StartGameUseCase createStartGameUseCase(StartGamePresenter presenter) {
        return new StartGameInteractor(presenter, gameDataAccess, new JavaRandomNumberGenerator());
    }

    public GuessUseCase createGuessUseCase(GuessPresenter presenter) {
        return new GuessInteractor(presenter, gameDataAccess);
    }

    public GetGameUseCase createGetGameUseCase(GetGamePresenter presenter) {
        return new GetGameInteractor(presenter, gameDataAccess);
    }

    public GetGamesGuessesUseCase createGetGamesGuessesUseCase(GetGamesGuessesPresenter presenter) {
        return new GetGamesGuessesInteractor(presenter, gameDataAccess);
    }
}
