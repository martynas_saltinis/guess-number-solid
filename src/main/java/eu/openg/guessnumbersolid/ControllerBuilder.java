package eu.openg.guessnumbersolid;

import eu.openg.guessnumbersolid.domain.usecase.GetGameUseCase;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesUseCase;
import eu.openg.guessnumbersolid.domain.usecase.GuessUseCase;
import eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase;
import eu.openg.guessnumbersolid.presentation.GetGameController;
import eu.openg.guessnumbersolid.presentation.GetGamesGuessesController;
import eu.openg.guessnumbersolid.presentation.GuessController;
import eu.openg.guessnumbersolid.presentation.StartGameController;
import eu.openg.guessnumbersolid.utils.JSONTransformer;

public class ControllerBuilder {
    private final JSONTransformer jsonTransformer;

    public ControllerBuilder() {
        jsonTransformer = new JSONTransformer();
    }

    public StartGameController createStartGameController(StartGameUseCase useCase) {
        return new StartGameController(useCase, jsonTransformer);
    }

    public GuessController createGuessController(GuessUseCase useCase) {
        return new GuessController(useCase, jsonTransformer);
    }

    public GetGameController createGetGameController(GetGameUseCase useCase) {
        return new GetGameController(useCase);
    }

    public GetGamesGuessesController createGetGamesGuessesController(GetGamesGuessesUseCase useCase) {
        return new GetGamesGuessesController(useCase);
    }
}
