package eu.openg.guessnumbersolid.rest;

import eu.openg.guessnumbersolid.ControllerBuilder;
import eu.openg.guessnumbersolid.PresenterBuilder;
import eu.openg.guessnumbersolid.UseCaseBuilder;
import eu.openg.guessnumbersolid.domain.usecase.GetGameUseCase;
import eu.openg.guessnumbersolid.presentation.GetGameController;
import spark.Request;
import spark.Response;
import spark.Route;

public class GetGameRoute implements Route {
    private final ControllerBuilder controllerBuilder;
    private final UseCaseBuilder useCaseBuilder;
    private final PresenterBuilder presenterBuilder;

    public GetGameRoute(ControllerBuilder controllerBuilder, UseCaseBuilder useCaseBuilder, PresenterBuilder presenterBuilder) {
        this.controllerBuilder = controllerBuilder;
        this.useCaseBuilder = useCaseBuilder;
        this.presenterBuilder = presenterBuilder;
    }


    @Override
    public Object handle(Request request, Response response) {
        GetGameUseCase useCase = useCaseBuilder.createGetGameUseCase(presenterBuilder.createGetGamePresenter(response));
        GetGameController controller = controllerBuilder.createGetGameController(useCase);
        controller.execute(request);
        return response.body();
    }
}
