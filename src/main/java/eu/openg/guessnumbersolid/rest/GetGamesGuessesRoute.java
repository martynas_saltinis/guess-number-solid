package eu.openg.guessnumbersolid.rest;

import eu.openg.guessnumbersolid.ControllerBuilder;
import eu.openg.guessnumbersolid.PresenterBuilder;
import eu.openg.guessnumbersolid.UseCaseBuilder;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesUseCase;
import eu.openg.guessnumbersolid.presentation.GetGamesGuessesController;
import spark.Request;
import spark.Response;
import spark.Route;

public class GetGamesGuessesRoute implements Route {
    private final ControllerBuilder controllerBuilder;
    private final UseCaseBuilder useCaseBuilder;
    private final PresenterBuilder presenterBuilder;

    public GetGamesGuessesRoute(ControllerBuilder controllerBuilder, UseCaseBuilder useCaseBuilder, PresenterBuilder presenterBuilder) {
        this.controllerBuilder = controllerBuilder;
        this.useCaseBuilder = useCaseBuilder;
        this.presenterBuilder = presenterBuilder;
    }

    @Override
    public Object handle(Request request, Response response) {
        GetGamesGuessesUseCase useCase = useCaseBuilder.createGetGamesGuessesUseCase(presenterBuilder.createGetGamesGuessesPresenter(response));
        GetGamesGuessesController controller = controllerBuilder.createGetGamesGuessesController(useCase);
        controller.execute(request);
        return response.body();
    }
}
