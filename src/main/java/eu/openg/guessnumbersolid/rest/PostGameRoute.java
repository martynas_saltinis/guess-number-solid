package eu.openg.guessnumbersolid.rest;

import eu.openg.guessnumbersolid.ControllerBuilder;
import eu.openg.guessnumbersolid.PresenterBuilder;
import eu.openg.guessnumbersolid.UseCaseBuilder;
import eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase;
import eu.openg.guessnumbersolid.presentation.StartGameController;
import spark.Request;
import spark.Response;
import spark.Route;

public class PostGameRoute implements Route {
    private final ControllerBuilder controllerBuilder;
    private final UseCaseBuilder useCaseBuilder;
    private final PresenterBuilder presenterBuilder;

    public PostGameRoute(ControllerBuilder controllerBuilder, UseCaseBuilder useCaseBuilder, PresenterBuilder presenterBuilder) {
        this.controllerBuilder = controllerBuilder;
        this.useCaseBuilder = useCaseBuilder;
        this.presenterBuilder = presenterBuilder;
    }

    @Override
    public Object handle(Request request, Response response) {
        StartGameUseCase useCase = useCaseBuilder.createStartGameUseCase(presenterBuilder.createStartGamePresenter(response));
        StartGameController controller = controllerBuilder.createStartGameController(useCase);
        controller.execute(request);
        return response.body();
    }
}
