package eu.openg.guessnumbersolid.rest;

import eu.openg.guessnumbersolid.ControllerBuilder;
import eu.openg.guessnumbersolid.PresenterBuilder;
import eu.openg.guessnumbersolid.UseCaseBuilder;
import eu.openg.guessnumbersolid.domain.usecase.GuessUseCase;
import eu.openg.guessnumbersolid.presentation.GuessController;
import spark.Request;
import spark.Response;
import spark.Route;

public class PostGuessRoute implements Route {
    private final ControllerBuilder controllerBuilder;
    private final UseCaseBuilder useCaseBuilder;
    private final PresenterBuilder presenterBuilder;

    public PostGuessRoute(ControllerBuilder controllerBuilder, UseCaseBuilder useCaseBuilder, PresenterBuilder presenterBuilder) {
        this.controllerBuilder = controllerBuilder;
        this.useCaseBuilder = useCaseBuilder;
        this.presenterBuilder = presenterBuilder;
    }

    @Override
    public Object handle(Request request, Response response) {
        GuessUseCase useCase = useCaseBuilder.createGuessUseCase(presenterBuilder.createGuessPresenter(response));
        GuessController controller = controllerBuilder.createGuessController(useCase);
        controller.execute(request);
        return response.body();
    }
}
