package eu.openg.guessnumbersolid;

public class HttpException extends RuntimeException {
    private final int status;

    public HttpException(int status, String message, Throwable e) {
        super(message, e);
        this.status = status;
    }

    public HttpException(int status, Throwable e) {
        super(e);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
