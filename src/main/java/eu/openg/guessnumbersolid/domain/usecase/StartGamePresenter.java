package eu.openg.guessnumbersolid.domain.usecase;

public interface StartGamePresenter {
    void onGameStart(StartGameResponse startInfo);

    class StartGameResponse {
        private final long gameId;
        private final int guessTo;
        private final int guessFrom;

        public StartGameResponse(long gameId, int guessFrom, int guessTo) {
            this.gameId = gameId;
            this.guessFrom = guessFrom;
            this.guessTo = guessTo;
        }

        public long getGameId() {
            return gameId;
        }

        public int getGuessTo() {
            return guessTo;
        }

        public int getGuessFrom() {
            return guessFrom;
        }
    }
}
