package eu.openg.guessnumbersolid.domain.usecase;

public interface GuessPresenter {
    void onSuccessfulGuess(GuessResponse guessResponse);

    void onGameNotFound();

    class GuessResponse {
        private final int answer;
        private final int numberOfGuesses;

        public GuessResponse(int answer, int numberOfGuesses) {
            this.answer = answer;
            this.numberOfGuesses = numberOfGuesses;
        }

        public int getAnswer() {
            return answer;
        }

        public int getNumberOfGuesses() {
            return numberOfGuesses;
        }
    }
}
