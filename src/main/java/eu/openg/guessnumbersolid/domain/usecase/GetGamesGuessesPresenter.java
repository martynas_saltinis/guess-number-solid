package eu.openg.guessnumbersolid.domain.usecase;

import java.util.List;

public interface GetGamesGuessesPresenter {
    void onSuccessfulGetGamesGuesses(GetGamesGuessesResponse response);

    void onGameNotFound();

    class GetGamesGuessesResponse {
        private final List<Guess> guesses;

        public GetGamesGuessesResponse(List<Guess> guesses) {
            this.guesses = guesses;
        }

        public List<Guess> getGuesses() {
            return guesses;
        }
    }

    class Guess {
        private final long id;
        private final int number;
        private final int answer;

        public Guess(long id, int number, int answer) {
            this.id = id;
            this.number = number;
            this.answer = answer;
        }

        public long getId() {
            return id;
        }

        public int getNumber() {
            return number;
        }

        public int getAnswer() { return answer; }
    }
}
