package eu.openg.guessnumbersolid.domain.usecase;

public interface StartGameUseCase {
    void startGame(StartGameRequest request);

    class StartGameRequest {
        private final int numberFrom;
        private final int numberTo;

        public StartGameRequest(int numberFrom, int numberTo) {
            this.numberFrom = numberFrom;
            this.numberTo = numberTo;
        }

        public int getNumberFrom() {
            return numberFrom;
        }

        public int getNumberTo() {
            return numberTo;
        }
    }
}
