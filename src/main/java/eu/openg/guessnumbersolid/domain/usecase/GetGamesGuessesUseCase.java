package eu.openg.guessnumbersolid.domain.usecase;

public interface GetGamesGuessesUseCase {
    void execute(long gameId);
}
