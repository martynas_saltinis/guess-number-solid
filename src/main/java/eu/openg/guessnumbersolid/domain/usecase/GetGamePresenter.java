package eu.openg.guessnumbersolid.domain.usecase;

public interface GetGamePresenter {
    void onSuccessfulGetGame(GetGameResponse getGameResponse);

    void onGameNotFound();

    class GetGameResponse {
        private final int numberOfGuesses;
        private final int guessFrom;
        private final int guessTo;

        public GetGameResponse(int numberOfGuesses, int guessFrom, int guessTo) {
            this.numberOfGuesses = numberOfGuesses;
            this.guessFrom = guessFrom;
            this.guessTo = guessTo;
        }

        public int getNumberOfGuesses() {
            return numberOfGuesses;
        }


        public int getGuessFrom() {
            return guessFrom;
        }

        public int getGuessTo() {
            return guessTo;
        }
    }
}
