package eu.openg.guessnumbersolid.domain.usecase;

public interface GetGameUseCase {
    void execute(long gameId);
}
