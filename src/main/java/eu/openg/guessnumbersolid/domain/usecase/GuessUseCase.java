package eu.openg.guessnumbersolid.domain.usecase;

public interface GuessUseCase {
    void execute(GuessRequest guessRequest);

    class GuessRequest {
        private final long gameId;
        private final int guess;

        public GuessRequest(long gameId, int guess) {
            this.gameId = gameId;
            this.guess = guess;
        }

        public long getGameId() {
            return gameId;
        }

        public int getGuess() {
            return guess;
        }
    }
}
