package eu.openg.guessnumbersolid.domain.entity;

import java.util.Objects;

public class Guess {
    private final long id;
    private final int number;

    public Guess(long id, int number) {
        this.id = id;
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Guess)) {
            return false;
        }
        Guess g = (Guess)o;
        return g.number == number && g.id == id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number);
    }
}
