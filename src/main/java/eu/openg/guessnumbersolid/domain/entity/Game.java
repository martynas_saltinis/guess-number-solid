package eu.openg.guessnumbersolid.domain.entity;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private final int answerFrom;
    private final int answerTo;
    private final long id;
    private final int answer;
    private List<Guess> guesses;

    public Game(long id, int answer, int answerFrom, int answerTo) {
        this(id, answer, answerFrom, answerTo, new ArrayList<>());
    }

    public Game(long id, int answer, int answerFrom, int answerTo, List<Guess> guesses) {
        this.answerFrom = answerFrom;
        this.answerTo = answerTo;
        this.id = id;
        this.answer = answer;
        this.guesses = guesses;
    }

    public GuessAnswer guess(Guess guess) {
        if (guesses.contains(guess))
            throw new GuessAlreadyExistsException(id, guess.getId());
        guesses.add(guess);
        return getAnswerToGuess(guess);
    }

    public long getId() {
        return id;
    }

    public List<Guess> getGuesses() {
        return guesses;
    }

    public int getAnswerFrom() {
        return answerFrom;
    }

    public int getAnswerTo() {
        return answerTo;
    }

    public GuessAnswer getAnswerToGuess(Guess guess) {
        if (guess.getNumber() == answer)
            return GuessAnswer.CORRECT;
        else if (guess.getNumber() < answer)
            return GuessAnswer.GUESS_HIGHER;
        else
            return GuessAnswer.GUESS_LOWER;
    }

    public enum GuessAnswer {
        GUESS_LOWER(-1),
        CORRECT(0),
        GUESS_HIGHER(1);

        private final int value;

        GuessAnswer(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    class GuessAlreadyExistsException extends RuntimeException {
        GuessAlreadyExistsException(long gameId, long guessId){
            super(String.format("Game with id (%d) already contains guess with id (%d).", gameId, guessId));
        }
    }
}
