package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesUseCase;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.GetGamesGuessesResponse;
import static eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.Guess;

public class GetGamesGuessesInteractor implements GetGamesGuessesUseCase {
    private final GetGamesGuessesPresenter presenter;
    private final GameDataAccess gameDataAccess;

    public GetGamesGuessesInteractor(GetGamesGuessesPresenter presenter, GameDataAccess gameDataAccess) {

        this.presenter = presenter;
        this.gameDataAccess = gameDataAccess;
    }

    @Override
    public void execute(long gameId) {
        Optional<Game> optionalGame = gameDataAccess.getGameById(gameId);
        if (optionalGame.isPresent())
            callPresenterWithGamesGuesses(optionalGame.get());
        else
            presenter.onGameNotFound();
    }

    private void callPresenterWithGamesGuesses(Game game) {
        List<Guess> responseGuesses = formGuessObjectsFromGamesGuesses(game);
        GetGamesGuessesResponse response = new GetGamesGuessesResponse(responseGuesses);
        presenter.onSuccessfulGetGamesGuesses(response);
    }

    private List<Guess> formGuessObjectsFromGamesGuesses(Game game) {
        return game.getGuesses().stream()
                .map(guess -> new Guess(guess.getId(), guess.getNumber(), game.getAnswerToGuess(guess).getValue()))
                .collect(Collectors.toList());
    }
}
