package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter;
import eu.openg.guessnumbersolid.domain.usecase.GetGameUseCase;

import java.util.Optional;

import static eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter.GetGameResponse;

public class GetGameInteractor implements GetGameUseCase {
    private final GetGamePresenter presenter;
    private final GameDataAccess gameDataAccess;

    public GetGameInteractor(GetGamePresenter presenter, GameDataAccess gameDataAccess) {
        this.presenter = presenter;
        this.gameDataAccess = gameDataAccess;
    }

    @Override
    public void execute(long gameId) {
        Optional<Game> optionalGame = gameDataAccess.getGameById(gameId);
        if (optionalGame.isPresent())
            callPresenterWithGameInfo(optionalGame.get());
        else
            presenter.onGameNotFound();
    }

    private void callPresenterWithGameInfo(Game game) {
        presenter.onSuccessfulGetGame(new GetGameResponse(
                game.getGuesses().size(),
                game.getAnswerFrom(),
                game.getAnswerTo())
        );
    }
}
