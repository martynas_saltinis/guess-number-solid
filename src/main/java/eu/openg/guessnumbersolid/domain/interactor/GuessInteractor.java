package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;
import eu.openg.guessnumbersolid.domain.entity.Game.GuessAnswer;
import eu.openg.guessnumbersolid.domain.entity.Guess;
import eu.openg.guessnumbersolid.domain.usecase.GuessPresenter;
import eu.openg.guessnumbersolid.domain.usecase.GuessUseCase;

import java.util.Optional;

import static eu.openg.guessnumbersolid.domain.usecase.GuessPresenter.GuessResponse;
import static java.util.Objects.requireNonNull;

public class GuessInteractor implements GuessUseCase {
    private final GuessPresenter presenter;
    private final GameDataAccess gameDataAccess;

    public GuessInteractor(GuessPresenter presenter, GameDataAccess gameDataAccess) {
        this.presenter = requireNonNull(presenter);
        this.gameDataAccess = requireNonNull(gameDataAccess);
    }

    @Override
    public void execute(GuessRequest guessRequest) {
        Optional<Game> optionalGame = gameDataAccess.getGameById(guessRequest.getGameId());
        if (optionalGame.isPresent())
            callPresenterWithGuessInfo(guessRequest, optionalGame.get());
        else
            presenter.onGameNotFound();
    }

    private void callPresenterWithGuessInfo(GuessRequest guessRequest, Game game) {
        GuessAnswer guessAnswer = makeAGuessThatReturnsAnswer(guessRequest, game);
        gameDataAccess.saveGame(game);
        presenter.onSuccessfulGuess(new GuessResponse(guessAnswer.getValue(), game.getGuesses().size()));
    }

    private GuessAnswer makeAGuessThatReturnsAnswer(GuessRequest guessRequest, Game game) {
        int currentGamesGuessesCount = game.getGuesses().size();
        Guess guess = new Guess(currentGamesGuessesCount + 1, guessRequest.getGuess());
        return game.guess(guess);
    }

}
