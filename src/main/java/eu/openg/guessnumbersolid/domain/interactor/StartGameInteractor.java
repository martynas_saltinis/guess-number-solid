package eu.openg.guessnumbersolid.domain.interactor;

import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.usecase.StartGamePresenter;
import eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase;
import eu.openg.guessnumbersolid.utils.AnswerGenerator;

import static eu.openg.guessnumbersolid.domain.usecase.StartGamePresenter.StartGameResponse;
import static java.util.Objects.requireNonNull;

public class StartGameInteractor implements StartGameUseCase {
    private final StartGamePresenter presenter;
    private final GameDataAccess gameDataAccess;
    private final AnswerGenerator answerGenerator;

    public StartGameInteractor(StartGamePresenter presenter, GameDataAccess gameDataAccess, AnswerGenerator answerGenerator) {
        this.presenter = requireNonNull(presenter);
        this.gameDataAccess = requireNonNull(gameDataAccess);
        this.answerGenerator = requireNonNull(answerGenerator);
    }

    @Override
    public void startGame(StartGameRequest request) {
        int numberFrom = request.getNumberFrom();
        int numberTo = request.getNumberTo();
        int answer = answerGenerator.generateAnswer(numberFrom, numberTo);
        long gameId = gameDataAccess.createNewGame(answer, numberFrom, numberTo);
        presenter.onGameStart(new StartGameResponse(gameId, numberFrom, numberTo));
    }
}
