package eu.openg.guessnumbersolid.domain.access;

import eu.openg.guessnumbersolid.domain.entity.Game;

import java.util.Optional;

public interface GameDataAccess {
    long createNewGame(int answer, int answerFrom, int answerTo);

    Optional<Game> getGameById(long gameId);

    void saveGame(Game game);
}
