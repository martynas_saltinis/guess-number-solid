package eu.openg.guessnumbersolid.presentation.converter;

import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.GetGamesGuessesResponse;
import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter.Guess;
import eu.openg.guessnumbersolid.presentation.GuessAnswerParser;

import java.util.ArrayList;
import java.util.List;

public class GetGamesGuessesResponseConverter {
    private final GuessAnswerParser guessAnswerParser;

    public GetGamesGuessesResponseConverter(GuessAnswerParser guessAnswerParser) {
        this.guessAnswerParser = guessAnswerParser;
    }

    public FormattedGetGamesGuessesResponse convert(GetGamesGuessesResponse response) {
        List<FormattedGuess> formattedGuesses = new ArrayList<>();
        for (Guess guess : response.getGuesses()) {
            String answer = guessAnswerParser.invoke(guess.getAnswer());
            formattedGuesses.add(new FormattedGuess(guess.getId(), guess.getNumber(), answer));
        }
        return new FormattedGetGamesGuessesResponse(formattedGuesses);
    }

    class FormattedGetGamesGuessesResponse {
        private final List<FormattedGuess> guesses;

        FormattedGetGamesGuessesResponse(List<FormattedGuess> guesses) {
            this.guesses = guesses;
        }

        public List<FormattedGuess> getGuesses() {
            return guesses;
        }
    }

    class FormattedGuess {
        private final long id;
        private final int number;
        private final String answer;

        FormattedGuess(long id, int number, String answer) {
            this.id = id;
            this.number = number;
            this.answer = answer;
        }

        public long getId() {
            return id;
        }

        public int getNumber() {
            return number;
        }

        public String getAnswer() {
            return answer;
        }
    }
}
