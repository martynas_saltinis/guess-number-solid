package eu.openg.guessnumbersolid.presentation.converter;

import eu.openg.guessnumbersolid.presentation.GuessAnswerParser;

import static eu.openg.guessnumbersolid.domain.usecase.GuessPresenter.GuessResponse;

public class GuessResponseConverter {
    private final GuessAnswerParser guessAnswerParser;

    public GuessResponseConverter(GuessAnswerParser guessAnswerParser) {
        this.guessAnswerParser = guessAnswerParser;
    }

    public FormattedGuessResponse convert(GuessResponse response) {
        return new FormattedGuessResponse(response.getNumberOfGuesses(), guessAnswerParser.invoke(response.getAnswer()));
    }

    class FormattedGuessResponse {
        private final int numberOfGuesses;
        private final String answer;

        public FormattedGuessResponse(int numberOfGuesses, String answer) {
            this.numberOfGuesses = numberOfGuesses;
            this.answer = answer;
        }

        public int getNumberOfGuesses() {
            return numberOfGuesses;
        }

        public String getAnswer() {
            return answer;
        }
    }
}
