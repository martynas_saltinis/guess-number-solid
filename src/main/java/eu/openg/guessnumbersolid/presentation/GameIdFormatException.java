package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.HttpException;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;

class GameIdFormatException extends HttpException {
    GameIdFormatException(String id, NumberFormatException e) {
        super(HttpProtocolUtil.BAD_REQUEST_STATUS, "Game id: " + id, e);
    }
}
