package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.HttpException;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JavaRandomNumberGenerator;
import eu.openg.guessnumbersolid.utils.JavaRandomNumberGenerator.InvalidIntervalSequence;

class InvalidRequestNumberInterval extends HttpException {
    private final int intervalStart;
    private final int intervalEnd;

    public InvalidRequestNumberInterval(InvalidIntervalSequence e) {
        super(HttpProtocolUtil.BAD_REQUEST_STATUS, String.format("Interval start (%d) is bigger than interval end (%d)", e.getIntervalStart(), e.getIntervalEnd()), e);
        intervalStart = e.getIntervalStart();
        intervalEnd = e.getIntervalEnd();
    }

    public int getIntervalStart() {
        return intervalStart;
    }

    public int getIntervalEnd() {
        return intervalEnd;
    }
}
