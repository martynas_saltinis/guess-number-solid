package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesUseCase;
import spark.Request;

public class GetGamesGuessesController {
    private final GetGamesGuessesUseCase useCase;

    public GetGamesGuessesController(GetGamesGuessesUseCase useCase) {
        this.useCase = useCase;
    }

    public void execute(Request request) {
        try {
            long gameId = Long.parseLong(request.params("id"));
            useCase.execute(gameId);
        } catch (NumberFormatException e) {
            throw new GameIdFormatException(request.params("id"), e);
        }
    }
}
