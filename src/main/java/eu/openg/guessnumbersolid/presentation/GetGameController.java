package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GetGameUseCase;
import spark.Request;


public class GetGameController {
    private final GetGameUseCase getGameUseCase;

    public GetGameController(GetGameUseCase getGameUseCase) {
        this.getGameUseCase = getGameUseCase;
    }

    public void execute (Request request) {
        try {
            long id = Long.parseLong(request.params("id"));
            getGameUseCase.execute(id);
        } catch (NumberFormatException e) {
            throw new GameIdFormatException(request.params("id"), e);
        }
    }
}
