package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GetGamePresenter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import spark.Response;

import static java.util.Objects.requireNonNull;

public class RestGetGamePresenter implements GetGamePresenter {
    private final Response viewModel;
    private final JSONTransformer jsonTransformer;

    public RestGetGamePresenter(Response viewModel, JSONTransformer jsonTransformer) {
        this.viewModel = viewModel;
        this.jsonTransformer = jsonTransformer;
    }

    @Override
    public void onSuccessfulGetGame(GetGameResponse getGameResponse) {
        requireNonNull(getGameResponse);
        viewModel.status(HttpProtocolUtil.SUCCESSFUL_GET_STATUS);
        viewModel.body(jsonTransformer.toJSON(getGameResponse));
    }

    @Override
    public void onGameNotFound() {
        viewModel.status(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS);
        viewModel.body("");
    }
}
