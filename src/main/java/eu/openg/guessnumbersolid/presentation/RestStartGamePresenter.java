package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.StartGamePresenter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import spark.Response;

import static eu.openg.guessnumbersolid.domain.usecase.StartGamePresenter.*;
import static java.util.Objects.requireNonNull;

public class RestStartGamePresenter implements StartGamePresenter {
    private final JSONTransformer jsonTransformer;
    private final Response viewModel;

    public RestStartGamePresenter(Response viewModel, JSONTransformer jsonTransformer) {
        this.viewModel = viewModel;
        this.jsonTransformer = jsonTransformer;
    }

    @Override
    public void onGameStart(StartGameResponse startInfo) {
        viewModel.body(jsonTransformer.toJSON(startInfo));
        viewModel.status(HttpProtocolUtil.SUCCESSFUL_POST_STATUS);
    }
}
