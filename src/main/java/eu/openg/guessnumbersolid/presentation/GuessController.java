package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GuessUseCase;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import spark.Request;

import static eu.openg.guessnumbersolid.domain.usecase.GuessUseCase.GuessRequest;

public class GuessController {
    private final GuessUseCase guessUseCase;
    private final JSONTransformer jsonTransformer;

    public GuessController(GuessUseCase guessUseCase, JSONTransformer jsonTransformer) {
        this.guessUseCase = guessUseCase;
        this.jsonTransformer = jsonTransformer;
    }

    public void execute(Request request) {
        RestGuess guess = jsonTransformer.fromJSON(request.body(), RestGuess.class);
        try {
            long id = Long.parseLong((request.params("id")));
            GuessRequest guessRequest = new GuessRequest(id, guess.getNumber());
            guessUseCase.execute(guessRequest);
        } catch (NumberFormatException e) {
            throw new GameIdFormatException(request.params("id"), e);
        }
    }
}
