package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GuessPresenter;
import eu.openg.guessnumbersolid.presentation.converter.GuessResponseConverter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import spark.Response;

import static java.util.Objects.requireNonNull;

public class RestGuessPresenter implements GuessPresenter {
    private final JSONTransformer jsonTransformer;
    private final GuessResponseConverter guessResponseConverter;
    private final Response viewModel;

    public RestGuessPresenter(Response viewModel, JSONTransformer jsonTransformer, GuessResponseConverter guessResponseConverter) {
        this.viewModel = viewModel;
        this.jsonTransformer = jsonTransformer;
        this.guessResponseConverter = guessResponseConverter;
    }

    @Override
    public void onSuccessfulGuess(GuessResponse guessResponse) {
        viewModel.status(HttpProtocolUtil.SUCCESSFUL_POST_STATUS);
        viewModel.body(jsonTransformer.toJSON(guessResponseConverter.convert(guessResponse)));
    }

    @Override
    public void onGameNotFound() {
        viewModel.status(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS);
        viewModel.body("");
    }
}
