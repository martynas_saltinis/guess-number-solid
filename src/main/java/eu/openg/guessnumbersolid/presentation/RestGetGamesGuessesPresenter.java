package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.GetGamesGuessesPresenter;
import eu.openg.guessnumbersolid.presentation.converter.GetGamesGuessesResponseConverter;
import eu.openg.guessnumbersolid.utils.HttpProtocolUtil;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import spark.Response;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class RestGetGamesGuessesPresenter implements GetGamesGuessesPresenter {
    private final Response viewModel;
    private final JSONTransformer jsonTransformer;
    private final GetGamesGuessesResponseConverter responseConverter;

    public RestGetGamesGuessesPresenter(Response viewModel, JSONTransformer jsonTransformer, GetGamesGuessesResponseConverter responseConverter) {
        this.viewModel = viewModel;
        this.jsonTransformer = jsonTransformer;
        this.responseConverter = responseConverter;
    }

    @Override
    public void onSuccessfulGetGamesGuesses(GetGamesGuessesResponse response) {
        viewModel.status(HttpProtocolUtil.SUCCESSFUL_GET_STATUS);
        viewModel.body(jsonTransformer.toJSON(responseConverter.convert(response)));
    }

    @Override
    public void onGameNotFound() {
        viewModel.status(HttpProtocolUtil.CONTENT_NOT_FOUND_STATUS);
        viewModel.body("");
    }
}
