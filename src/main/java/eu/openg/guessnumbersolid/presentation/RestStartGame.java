package eu.openg.guessnumbersolid.presentation;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RestStartGame {
    private final int numberFrom;
    private final int numberTo;

    public RestStartGame(@JsonProperty(value = "numberFrom", required = true) int numberFrom,
                         @JsonProperty(value = "numberTo", required = true) int numberTo) {
        this.numberFrom = numberFrom;
        this.numberTo = numberTo;
    }

    public int getNumberFrom() {
        return numberFrom;
    }

    public int getNumberTo() {
        return numberTo;
    }
}
