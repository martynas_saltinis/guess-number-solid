package eu.openg.guessnumbersolid.presentation;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RestGuess {
    private final int number;

    public RestGuess(@JsonProperty(value = "number", required = true) int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
