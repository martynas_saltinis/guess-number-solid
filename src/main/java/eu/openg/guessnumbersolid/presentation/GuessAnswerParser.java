package eu.openg.guessnumbersolid.presentation;

public class GuessAnswerParser {
    public String invoke(int numericAnswer) {
        switch (numericAnswer) {
            case -1:
                return "Guess lower";
            case 0:
                return "Correct";
            case 1:
                return "Guess higher";
            default:
                return "";
        }
    }
}
