package eu.openg.guessnumbersolid.presentation;

import eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase;
import eu.openg.guessnumbersolid.utils.JSONTransformer;
import eu.openg.guessnumbersolid.utils.JavaRandomNumberGenerator.InvalidIntervalSequence;
import spark.Request;

import static eu.openg.guessnumbersolid.domain.usecase.StartGameUseCase.StartGameRequest;
import static java.util.Objects.requireNonNull;

public class StartGameController {
    private final StartGameUseCase startGameUseCase;
    private final JSONTransformer jsonTransformer;

    public StartGameController(StartGameUseCase startGameUseCase, JSONTransformer jsonTransformer) {
        this.startGameUseCase = startGameUseCase;
        this.jsonTransformer = jsonTransformer;
    }

    public void execute(Request request) {
        RestStartGame restStartGame = jsonTransformer.fromJSON(request.body(), RestStartGame.class);
        StartGameRequest startGameRequest = new StartGameRequest(restStartGame.getNumberFrom(), restStartGame.getNumberTo());
        try {
            startGameUseCase.startGame(startGameRequest);
        } catch (InvalidIntervalSequence e) {
            throw new InvalidRequestNumberInterval(e);
        }
    }


}
