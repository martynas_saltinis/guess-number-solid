package eu.openg.guessnumbersolid;

import eu.openg.guessnumbersolid.rest.GetGameRoute;
import eu.openg.guessnumbersolid.rest.GetGamesGuessesRoute;
import eu.openg.guessnumbersolid.rest.PostGameRoute;
import eu.openg.guessnumbersolid.rest.PostGuessRoute;
import spark.Response;
import spark.Service;

import static eu.openg.guessnumbersolid.utils.HttpProtocolUtil.CONTENT_TYPE_JSON;

public class ApplicationRunner {
    private final Service service;

    public ApplicationRunner() {
        this.service = Service.ignite();
    }

    public static void main(String... args) {
        Config config = new Config();
        new ApplicationRunner().start(4567, config);
    }

    public void start(int port, Config config) {
        service.port(port);
        service.before("*", (req, res) -> res.type(CONTENT_TYPE_JSON));
        service.post("/games", new PostGameRoute(new ControllerBuilder(), new UseCaseBuilder(config.getDataAccess()), new PresenterBuilder()));
        service.get("/games/:id", new GetGameRoute(new ControllerBuilder(), new UseCaseBuilder(config.getDataAccess()), new PresenterBuilder()));
        service.post("/games/:id/guesses", new PostGuessRoute(new ControllerBuilder(), new UseCaseBuilder(config.getDataAccess()), new PresenterBuilder()));
        service.get("/games/:id/guesses", new GetGamesGuessesRoute(new ControllerBuilder(), new UseCaseBuilder(config.getDataAccess()), new PresenterBuilder()));
        service.exception(HttpException.class, (e, req, res) -> attachToResponse(e, res));
        service.awaitInitialization();
    }

    private void attachToResponse(HttpException e, Response res) {
        res.type(CONTENT_TYPE_JSON);
        res.status(e.getStatus());
        res.body("");
    }

    public int getPort() {
        return service.port();
    }

    public void stop() {
        service.stop();
    }
}
