package eu.openg.guessnumbersolid;

import eu.openg.guessnumbersolid.data.InMemoryGameDataAccess;
import eu.openg.guessnumbersolid.domain.access.GameDataAccess;
import eu.openg.guessnumbersolid.domain.entity.Game;

import java.util.ArrayList;
import java.util.List;

public class Config {
    private final GameDataAccess gameDataAccess;

    public Config() {
        this(new ArrayList<>());
    }

    public Config(List<Game> games) {
        gameDataAccess = new InMemoryGameDataAccess(games);
    }

    public GameDataAccess getDataAccess() {
        return gameDataAccess;
    }
}
